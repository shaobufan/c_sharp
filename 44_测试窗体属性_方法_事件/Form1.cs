namespace _44_测试窗体属性_方法_事件
{
    public partial class Form1 : Form
    {
        static int x = 200;
        static int y = 200;
        static int count = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            count += 1;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            count -= 1;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("确认关闭吗?", "确认", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                e.Cancel = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 form2 = new Form1();
            form2.FormBorderStyle = FormBorderStyle.Fixed3D;
            form2.Cursor = Cursors.Hand;
            form2.SetDesktopLocation(x, y);
            form2.Visible = true;
            x += 30;
            y += 30;
            this.Activate();
            this.button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            label1.Text = "新窗体的坐标(" + x + "," + y + ")";
            label2.Text = "这是第" + count + "个窗体";
        }
    }
}