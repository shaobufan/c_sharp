﻿namespace Example
{
    public class Program
    {
        static double SafeDivsion(double x, double y)
        {
            if (y == 0)
                throw new System.DivideByZeroException();
            return x / y;
        }
        public static void Main(string[] args)
        {
            double a = 98, b = 0;
            double result = 0;
            try
            {
                result = SafeDivsion(a, b);
                Console.WriteLine($"{a}除以{b}={result}");
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e);
                Console.WriteLine("尝试除以零");
            }
        }
    }
}