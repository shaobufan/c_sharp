namespace _47_钟表绘制
{
    public partial class Form1 : Form
    {
        bool fIsMouseDown = false;
        Point fDownPosition;
        public Form1()
        {
            InitializeComponent();

            System.Drawing.Drawing2D.GraphicsPath formShape = new System.Drawing.Drawing2D.GraphicsPath();
            formShape.AddEllipse(0, 0, 400, 400);
            this.Region = new System.Drawing.Region(formShape);

            Bitmap Image1 = Properties.Resources.表;

            Bitmap Image2 = new Bitmap(Image1, 400, 400);
            this.BackgroundImage = Image2;
        }

        private void fDrawDot(Graphics aGraphics, double aValue, int aRadius)
        {
            double lAngle = ((360 - aValue * 6) + 90) * Math.PI / 180;
            Point lPoint = new Point((int)(200 + 180 * Math.Cos(lAngle)), (int)(200 - 180 * Math.Sin(lAngle)));
            aGraphics.FillEllipse(Brushes.Black, new Rectangle(lPoint.X - aRadius, lPoint.Y - aRadius, 2 * aRadius, 2 * aRadius));
        }

        private void fDrawLine(Graphics aGraphics, double aValue, double aLength, Pen aPen)
        {
            double lAngle = ((360 - aValue * 6) + 90) * Math.PI / 180;
            Point lPoint = new Point((int)(200 + aLength * Math.Cos(lAngle)), (int)(200 - aLength * Math.Sin(lAngle)));
            aGraphics.DrawLine(aPen, new Point(200, 200), lPoint);
        }

        private void frmTry_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            e.Graphics.DrawImage(Properties.Resources.表, 0, 0, 400, 400);
            for (int i = 0; i < 60; i++)
            {
                if (i % 5 == 0)
                    fDrawDot(e.Graphics, i, 6);
                else
                    fDrawDot(e.Graphics, i, 2);
            }
            DateTime dt = DateTime.Now;
            Pen pSecond = new Pen(Color.Yellow, 2);
            Pen pSMinute = new Pen(Color.Red, 2);
            Pen pHour = new Pen(Color.Blue, 4);

            fDrawLine(e.Graphics, ((dt.Hour % 12)) * 5, 100, pHour);
            fDrawLine(e.Graphics, dt.Minute, 160, pSMinute);
            fDrawLine(e.Graphics, dt.Second + dt.Millisecond / 1000.0, 170, pSecond);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button== MouseButtons.Left)
            {
                fIsMouseDown = true;
                fDownPosition = e.Location;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if(fIsMouseDown)
            {
                this.Left = this.Location.X + (e.X - fDownPosition.X);
                this.Top = this.Location.Y + (e.Y - fDownPosition.Y);
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            fIsMouseDown = false;
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}