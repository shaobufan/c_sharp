﻿namespace program
{
    public class Example
    {
        static void outTest(out int x, out int y)
        {
            x = 1;
            y = 2;
        }
        static void refTest(ref int x, ref int y)
        {
            x = 1;
            y = 2;
        }
        public static void Main(string[] args)
        {
            int a, b;
            int a, b;
            outTest(out a, out b);
            Console.WriteLine("a={0};b={1}", a, b);

            int c = 11, d = 22;
            outTest(out c, out d);
            Console.WriteLine("c={0};d={1}", c, d);

            //int m, n;
            int o = 11, p = 22;
            refTest(ref o, ref p);
            Console.WriteLine("o={0};p={1}", o, p);
            //Console.WriteLine("m={0};n={1}", m, n);
        }
    }
}