﻿namespace Example
{
    public class Program
    {
        static void Fun1()
        {
            try
            {
                throw new Exception();
            }
            catch
            {
                Console.WriteLine("Fun1 已运行");
            }
            Console.WriteLine("Fun1 运行结束");
        }

        static void Fun2(bool arg)
        {
            if (arg)
                throw new Exception();
            Console.WriteLine("Fun2 运行结束");
        }

        static void Fun3()
        {
            try
            {
                Console.WriteLine("运行 Fun1");
                Fun1();
                Console.WriteLine("运行 Fun2");
                Fun2(true);
            }
            catch
            {
                Console.WriteLine("Fun3 已处理");
            }
            Console.WriteLine("Fun3 运行结束");
        }

        public static void Main(string[] args)
        {
            Fun3();
        }
    }
}
