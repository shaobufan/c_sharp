﻿namespace program
{
    class ClassA { }
    class ClassB { }
    class Example
    {
        public static void Main(string[] args)
        {
            object?[] objArray = new object?[6];
            
            objArray[0] = new ClassA();
            objArray[1] = new ClassB();
            objArray[2] = "hello";
            objArray[3] = 123;
            objArray[4] = 123.4;
            objArray[5] = null;

            for (int i = 0; i < objArray.Length; i++)
            {
                Console.Write($"{i}:");
                if (objArray[i] as String != null)
                {
                    Console.WriteLine($"\"{objArray[i] as String}\"");
                }
                else
                {
                    Console.WriteLine("转换String失败");
                }
            }
        }
    }
}