﻿namespace program
{
    class StaticExample
    {
        public static int sta1 = 10;
        public int a2 = 10;
    }
    public class Example
    {
        public static void Main(string[] args)
        {
            StaticExample A, B;
            A = new StaticExample();
            B = new StaticExample();

            StaticExample.sta1 = StaticExample.sta1 + 10;
            A.a2 = A.a2 + 10;

            StaticExample.sta1 = StaticExample.sta1 + 10;
            B.a2 = B.a2 + 10;

             ("StaticExample.sta1={0},B.a2={1}", StaticExample.sta1, B.a2);
        }
    }
}
