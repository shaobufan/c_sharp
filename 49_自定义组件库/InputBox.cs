﻿namespace _49_自定义组件库
{
    public partial class InputBox : UserControl
    {
        public InputBox()
        {
            InitializeComponent();
        }

        public string InputText
        {
            get { return txtContent.Text; }
            set { txtContent.Text = value; }
        }
        public string InputHinit
        {
            get { return lblHint.Text; }
            set { lblHint.Text = value; }
        }
    }
}