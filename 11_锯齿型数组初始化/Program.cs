﻿using System;
namespace Exanple
{
    public class example
    {
        public static void Main(string[] args)
        {
            String[][] a = new String[2][];
            a[0] = new String[5] { "1", "3", "5", "7", "9" };
            a[1] = new String[4] { "0", "2", "4", "6" };
            a[0][0] = "1";
            Random randObj = new Random();

            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < a[i].Length; j++)
                {
                    a[i][j] = randObj.Next(10, 99).ToString();
                }
            }

            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < a[i].Length; j++)
                {
                    Console.WriteLine("a[" + i + "]" + "[" + j + "] =" + a[i][j]);
                }
            }
        }
    }
}