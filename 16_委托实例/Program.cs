﻿using System;
namespace Example
{
    public delegate string myDelegate(string name);
    public class Program
    {
        public static string FunctionA(string name)
        {
            return "A say Hello to" + name;
        }
        public static string FunctionB(string name)
        {
            return "B say Hello to" + name;
        }
        public static void MethodA(myDelegate Me)
        {
            Console.WriteLine(Me("张三"));
        }
        public static void Main()
        {
            myDelegate a = new myDelegate(FunctionA);
            myDelegate b = new myDelegate(FunctionB);
            MethodA(a);
            MethodA(b);
        }
    }
}