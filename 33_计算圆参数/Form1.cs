namespace _33_计算圆参数
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = (2 * Math.PI * double.Parse(textBox1.Text)).ToString();
            textBox3.Text = (Math.PI * double.Parse(textBox1.Text) * double.Parse(textBox1.Text)).ToString();
            textBox4.Text = ((4.0 / 3.0) * Math.PI * double.Parse(textBox1.Text) * double.Parse(textBox1.Text) * double.Parse(textBox1.Text)).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}