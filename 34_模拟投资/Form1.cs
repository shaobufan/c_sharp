namespace _34_模拟投资
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int a, b, c, d;
            a = r.Next(8);
            b = r.Next(8);
            c = r.Next(8);
            d = int.Parse(textBox1.Text);
            textBox2.Text = a.ToString();
            textBox3.Text = b.ToString();
            textBox4.Text = c.ToString();
            if (a == b && b == c)
                d += a * 10;
            else
                d -= 2;

            textBox1.Text = d.ToString();
            if(d<10)
                textBox1.ForeColor = Color.Red;
            else
                textBox1.ForeColor = Color.Black;

            if(d<2)
                button1.Enabled = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}