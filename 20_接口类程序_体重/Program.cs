﻿namespace Example
{
    interface Life
    {
        void Eat(double food);
    }

    class Person : Life
    {
        public double Weight;
        public void Eat(double food)
        {
            Weight += food;
        }
    }

    public class example
    {
        public static void Main(string[] args)
        {
            Person person = new Person();
            person.Weight = 80.1;
            Console.WriteLine($"饭前的体重:{person.Weight}");
            person.Eat(4.3);
            person.Eat(3.2);
            Console.WriteLine($"饭后的体重:{person.Weight}");
        }
    }
}