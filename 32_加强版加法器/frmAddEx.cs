namespace _32_加强版加法器
{
    public partial class frmAddEx : Form
    {
        //int num = 0;
        public frmAddEx()
        {
            InitializeComponent();
            int n;
            Random randomNumber = new Random();
            n = randomNumber.Next(10);
            lblOp1.Text = n.ToString();
            n = randomNumber.Next(10);
            lblOp2.Text = n.ToString();
        }
        private void frmAddEx_Load(object sender, EventArgs e)
        {

        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            int x = r.Next(1, 100);
            int y = r.Next(1, 100);
            lblOp1.Text = x.ToString();
            lblOp2.Text = y.ToString();
        }
        private void btnJudge_Click(object sender, EventArgs e)
        {
            //num += 1;
            //sum.Text = "第"+num.ToString()+"题";
            if (this.txtResult.Text == "")
            {
                MessageBox.Show("你未输入答案");
                return;
            }
            int a, b, c;
            a = int.Parse(lblOp1.Text);
            b = int.Parse(lblOp2.Text);
            c = int.Parse(txtResult.Text);

            if (c == a + b)
            {
                MessageBox.Show("答对了");

                frmAddEx_Load(null, null);

                txtResult.Text = "";
                txtResult.Focus();
            }
            else
            {
                MessageBox.Show("答错了");
                txtResult.Text = "";
                txtResult.Focus();
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void txtResult_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) == true || e.KeyChar == 8))
                e.Handled = true;

            if (e.KeyChar == 13)
            {
                //num += 1;
                //sum.Text = "第" + num.ToString() + "题";
                if (this.txtResult.Text == "")
                {
                    MessageBox.Show("你未输入答案");
                    return;
                }

                int a, b, c;
                a = int.Parse(lblOp1.Text);
                b = int.Parse(lblOp2.Text);
                c = int.Parse(txtResult.Text);

                if (c == a + b)
                {
                    MessageBox.Show("答对了");

                    frmAddEx_Load(null, null);

                    txtResult.Text = "";
                    txtResult.Focus();
                }
                else
                {
                    MessageBox.Show("答错了");
                    txtResult.Text = "";
                    txtResult.Focus();
                }
            }
        }
    }
}