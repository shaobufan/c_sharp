namespace _31_加法器练习
{
    public partial class frmMain : Form
    {
        Random r = new Random();
        int Op1, Op2;

        public frmMain()
        {
            InitializeComponent();
        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            Op1 = r.Next(100);
            Op2 = r.Next(100 - Op1);
            lblOp1.Text = Op1.ToString();
            lblOp2.Text = Op2.ToString();
            txtResult.Text = string.Empty;
        }
        private void btnJudge_Click(object sender, EventArgs e)
        {
            try
            {
                int result = int.Parse(txtResult.Text);
                if (result == Op1 + Op2)
                    MessageBox.Show("正确");
                else
                    MessageBox.Show("错误");
            }
            catch (System.FormatException)
            {
                MessageBox.Show("输入错误");
                return;
            }
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblAdd_Click(object sender, EventArgs e)
        {

        }
        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}