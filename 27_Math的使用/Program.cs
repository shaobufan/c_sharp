﻿namespace program
{
    class Example
    {
        public static void Main(string[] args)
        {
            int i = 10, j = -5;
            double x = 1.3, y = 2.7;
            double a = 2.0, b = 5.0;

            Console.WriteLine($"-5的绝对值为{Math.Abs(j)}");
            Console.WriteLine($"大于等于1.3的最小整数为{Math.Ceiling(x)}");
            Console.WriteLine($"小于等于2.7的最大整数为{Math.Floor(y)}");
            Console.WriteLine($"10和5的较大者为{Math.Max(i, j)}");
            Console.WriteLine($"1.3和2.7的较小者为{Math.Min(x, y)}");
            Console.WriteLine($"2的5次方为{Math.Pow(a, b)}");
            Console.WriteLine($"1.3的四舍五入的值为{Math.Round(x)}");
            Console.WriteLine($"5的算数平方根为{Math.Sqrt(b)}");
        }
    }
}
