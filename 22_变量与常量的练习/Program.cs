﻿namespace program
{
    public class Example
    {
        static void Fun(int a, ref int b, ref int c, out int d, out int e)
        {
            int Temp;

            if (b < c)
            {
                d = b;
                e = c;
            }
            else
            {
                d = c;
                e = b;
            }

            Temp = b;
            b = c;
            c = Temp;

            d = d * a;
            e = e * a;
        }
        public static void Main(string[] args)
        {
            int a = 2, b = 4, c = 8, d, e;
            Fun(a, ref b, ref c, out d, out e);
            Console.WriteLine("b:{0}, c:{1}, d:{2}, e:{3}", b, c, d, e);
            
            //int x = 5;
            //int y = x++;
            //Console.Write(y + " ");
            //y = ++x;
            //Console.Write(y);
            //int a = 3 + 1 > 5 ? 0 : 1;
            //Console.WriteLine(a);
            //int a = 2;
            //a += 3;
            //Console.WriteLine(a);
            //int x = 10;
            //Console.WriteLine(x < 10 ? x = 0 : x++);
            //int x = 5;
            //x -= 3;
            //Console.WriteLine(x);
            //Console.WriteLine(4 * 10 >= 65);
            //Console.WriteLine(-5 % 3);
            //Console.WriteLine(true && true);
        }
    }
}