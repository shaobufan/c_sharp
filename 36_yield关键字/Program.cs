﻿using System.Collections;
using System.Threading;
namespace Example
{
    public class persons : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return "1";
            Thread.Sleep(1000);
            yield return "2";
            Thread.Sleep(1000);
            yield return "3";
            Thread.Sleep(1000);
            yield return "4";
            Thread.Sleep(1000);
            yield return "5";
            Thread.Sleep(1000);
            yield return "6";
        }
    }
    public class Program
    {
        public static IEnumerable Power(int num, int exp)
        {
            int result = 1;
            for (int i = 0; i < exp; i++)
            {
                result *= num;
                yield return result;
            }
        }
        public static void Main(string[] args)
        {
            foreach (int val in Power(2, 10))
                Console.WriteLine(val);

            persons arrPersons = new persons();
            foreach (string s in arrPersons)
            {
                Console.WriteLine(s);
            }
        }
    }
}