﻿using System;
namespace Example
{
    public class example
    {
        
        public static void Main(string[] args)
        {
            String? a = Console.ReadLine();
            Console.WriteLine(a);

            String b = Console.ReadLine()!;
            Console.WriteLine(b);

            String c = Console.ReadLine() ?? "空值";
            Console.WriteLine(c);

            bool bol = int.TryParse(Console.ReadLine(), out int d);
            Console.WriteLine(d);

            bol = bool.TryParse(Console.ReadLine(), out bool bol_1);
            Console.WriteLine(bol_1);

            int e = int.Parse(Console.ReadLine() ?? "0");
        }
    }
}