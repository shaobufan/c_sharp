﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _41_颜色改变窗体
{
    public partial class dlgColorSelector : Form
    {
        public dlgColorSelector()
        {
            InitializeComponent();
        }

        public Color SelectedColor
        {
            get
            {
                return Color.FromArgb((int)nudRed.Value, (int)nudGreen.Value, (int)nudBlue.Value);
            }
            set
            {
                nudRed.Value = value.R;
                nudGreen.Value = value.G;
                nudBlue.Value = value.B;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void dlgColorSelector_Load
        (object sender, EventArgs e)
        {

        }
    }
}
