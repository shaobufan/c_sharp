namespace _41_颜色改变窗体
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnSelectColor_Click(object sender, EventArgs e)
        {
            dlgColorSelector cs = new dlgColorSelector();
            cs.SelectedColor = BackColor;
            if (cs.ShowDialog() == DialogResult.OK)
                BackColor = cs.SelectedColor;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }
    }
}