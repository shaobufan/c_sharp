﻿namespace _49_1_应用组件库
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ibC = new _49_自定义组件库.InputBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ibF = new _49_自定义组件库.InputBox();
            this.SuspendLayout();
            // 
            // ibC
            // 
            this.ibC.InputHinit = "摄氏度";
            this.ibC.InputText = "";
            this.ibC.Location = new System.Drawing.Point(12, 12);
            this.ibC.Name = "ibC";
            this.ibC.Size = new System.Drawing.Size(191, 33);
            this.ibC.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 29);
            this.button1.TabIndex = 1;
            this.button1.Text = "转换";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ibF
            // 
            this.ibF.InputHinit = "华氏度";
            this.ibF.InputText = "";
            this.ibF.Location = new System.Drawing.Point(12, 66);
            this.ibF.Name = "ibF";
            this.ibF.Size = new System.Drawing.Size(191, 38);
            this.ibF.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 116);
            this.Controls.Add(this.ibF);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ibC);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private _49_自定义组件库.InputBox ibC;
        private Button button1;
        private _49_自定义组件库.InputBox ibF;
    }
}