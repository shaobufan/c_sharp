namespace _49_1_应用组件库
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double c = double.Parse(ibC.InputText);
                double f = 32 + c * 1.8;
                ibF.InputText = f.ToString();
            }
            catch
            {
                MessageBox.Show("无效输入");
            }
        }
    }
}