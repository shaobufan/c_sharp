﻿using System;
namespace Example
{
    public class Area
    {
        public static void Main(string[] args)
        {
            string[] sArray1 = { "少", "不", "钒" };
            string s1 = string.Join(",", sArray1);
            string[] sArray2 = s1.Split(',');
            string s2 = "少 不;钒,陈";
            string[] sArray3 = s2.Split(',', ';', ' ');
            Console.WriteLine(s1);

            Console.WriteLine(string.Join(Environment.NewLine, sArray3));
        }
    }
}