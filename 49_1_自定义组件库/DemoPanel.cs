﻿namespace _49_1_自定义组件库
{
    public partial class DemoPanel : UserControl
    {
        private int[] fData;
        private int fInitLength = 20;
        private int fIncLength = 2;
        public DemoPanel()
        {
            InitializeComponent();
            fData = new int[this.Width / 3];
            for (int i = 0; i < fData.Length; i++)
            {
                fData[i] = fInitLength + i * fIncLength;
            }
        }

        public int Length
        {
            get { return fData.Length; }
        }
        string fLineBase = "Bottom";
        public string IineBase
        {
            get { return fLineBase; }
            set
            {
                if(value == "left"||value == "Bottom")
                    fLineBase = value;
            }
        }
        public void RePaint(int i)
        {
            this.Invalidate(new Rectangle(3 * i, 0, 2, this.Height));
            this.Update();
        }
        public int this[int index]
        {
            get { return fData[index]; }
            set
            {
                fData[index] = value;
                RePaint(index);
            }
        }

        private void DemoPanel_Paint(object sender, PaintEventArgs e)
        {
            for(int i = 0;i<fData.Length;i++)
            {
                e.Graphics.FillRectangle(Brushes.BlueViolet, 3 * i, fLineBase == "Top" ? 0 : this.Height - fData[i], 2, fData[i]);
            }
        }
    }
}