﻿namespace Example
{
    class TriangleArgumentException : Exception { }
    class InvalidArgumentException : Exception { }
    public class Program
    {
        public static double Area(double a, double b, double c)
        {
            if (a <= 0 || b <= 0 || c < 0)
            {
                throw new TriangleArgumentException();
            }
            if (!(a + b > c && b + c > a && a + c > b))
            {
                throw new TriangleArgumentException();
            }

            double s = (a + b + c) / 2;
            return Math.Sqrt(s * (s - a) + s * (s - b) + s * (s - c));
        }

        public static int number(int a)
        {
            if (a > 100)
            {
                throw new InvalidArgumentException();
            }
            else
                return a;
        }
        
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine(Area(1, 1, 1));
                Console.WriteLine(Area(1, 0, 1));
                Console.WriteLine(number(10));
                Console.WriteLine(number(120));
            }
            catch (TriangleArgumentException e)
            {
                Console.WriteLine(e);
                Console.WriteLine("非法");
            }
            catch (InvalidArgumentException e)
            {
                Console.WriteLine(e);
                Console.WriteLine("超过阈值");
            }
        }
    }
}