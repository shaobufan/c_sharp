﻿using System;
namespace Example_2
{
    public class Max
    {
        public static void Main(string[] args)
        {
            Double n1, n2, n3, max;
            bool b1, b2, b3;
            Console.Write("Num 1:");
            b1 = double.TryParse(Console.ReadLine(), out n1);
            max = n1;
            Console.Write("Num 2:");
            b2 = double.TryParse(Console.ReadLine(), out n2);
            if (max < n2)
                max = n2;
            Console.Write("Num 3:");
            b3 = double.TryParse(Console.ReadLine(), out n3);
            if (max < n3)
                max = n3;
            Console.WriteLine("{0},{1},{2} 的最大值为:{3}", n1, n2, n3, max);
        }
    }
}