﻿using ClassLibrary;
namespace Program
{
    class Example
    {
        public static void Main(string[] args)
        {
            Console.Write("字符串加密前:");

            string s = Console.ReadLine() ?? "空";
            string? e = Functions.Encrypt(s);
            Console.WriteLine($"字符串加密后:{e}");

            string? d = Functions.Decrypt(e!);
            Console.WriteLine($"字符串解密后:{d}");
        }
    }
}