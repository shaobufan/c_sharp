﻿using System;
class Example
{
    public static void Main()
    {
        String? a;
        int n1 = 0, n2 = 0, n3 = 0;
        a = Console.ReadLine();
        if (a != null)
        {
            a = a.Trim();
            for (int i = 0; i < a.Length; i++)
            {
                char c = char.Parse(a.Substring(i, 1));
                if (char.IsDigit(c))
                    n2++;
                else if (char.IsLetter(c))
                    n1++;
                else
                    n3++;
            }
        }
        Console.WriteLine("数字字符有{0}个", n2);
        Console.WriteLine("字母字符有{0}个", n1);
        Console.WriteLine("其他字符有{0}个", n3);
    }
}