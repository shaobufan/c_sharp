﻿using System;
namespace Example
{
    public class example
    {
        public static void Main(string[] args)
        {
            int[] a = { 10, 20, 4, 8 };
            string[] s = GetString(a);
            Console.WriteLine("初始值:{0}", string.Join(",", s));
            Console.WriteLine("平均值:{0}", a.Average());
            Console.WriteLine("和:{0}", a.Sum());
            Console.WriteLine("最大值:{0}", a.Max());
            Console.WriteLine("最小值:{0}", a.Min());
        }
        static string[] GetString(int[] a)
        {
            string[] s = new string[a.Length];
            for (int i = 0; i < s.Length; i++)
            {
                s[i] = a[i].ToString();
            }
            return s;
        }
    }
}
