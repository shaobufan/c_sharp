﻿namespace Program
{
    public class Example
    {
        public static void Main(string[] args)
        {
            int[] a = { 1, 2, 3, 4, 5, 6 };
            int Max, Min;
            mInAx(a, out Max, out Min);
            Console.WriteLine("最小值:{0}\n最大值:{1}", Min, Max);
        }
        public static void mInAx(int[] arg, out int max, out int min)
        {
            int temp;
            int len = arg.Length;
            for (int i = 0; i < len; i++)
            {
                for (int j = len - 1; j > i; j--)
                {
                    if (arg[j] < arg[j - 1])
                    {
                        temp = arg[j];
                        arg[j] = arg[j - 1];
                        arg[j - 1] = temp;
                    }
                }
            }
            max = arg[len - 1];
            min = arg[0];
        }
    }
}