﻿using System;
namespace Example
{
    public class enumerate
    {
        enum ErrorCode : ushort
        {
            None = 0,
            Unknown = 1,
            ConnectionLost = 100,
            OutlierReading = 200
        }
        enum Colors { Red = 1, Green = 2, Blue = 4, Yellow = 8 };
        static void Main(string[] args)
        {
            //Colors mycolor1 = (Colors)Enum.Parse(typeof(Colors), "8");
            Colors mycolor1 = (Colors)Enum.Parse(typeof(Colors), "Yellow");
            // Color mycolor1 = (Color)Enum.Parse(typeof(Color),8); 参数8必须为字符串
            String mycolor = Console.ReadLine();
            Console.WriteLine(mycolor1);
            Type a = Type.GetType("Example.enumerate+Colors");//定义type变量,手动转换类型
            if (mycolor != null)
            {
                Colors mycolor2 = (Colors)Enum.Parse(a, mycolor);// type a 为枚举类的类型
                Console.WriteLine(mycolor2);
            }
        }
    }
}