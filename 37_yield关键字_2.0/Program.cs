﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    public class Program
    {
        public class MusicTitles
        {
            string[] names = { "Tubular Bells", "Hergest Ridge", "Ommadawn", "Platinum" };

            ////数组中前三个的迭代
            public System.Collections.IEnumerator GetEnumerator()
            {
                for (int i = 0; i < 4; i++)
                {
                    if (i == 3)
                    { yield break; }
                    yield return names[i];
                }
            }

            /// 用Reverse()方法逆序迭代标题
            public System.Collections.IEnumerable Reverse()
            {
                for (int i = 2; i >= 0; i--)
                    yield return names[i];
            }

            /// 用Subset()方法搜索迭代子集
            /// index 索引起始位置，length 元素个数
            public System.Collections.IEnumerable Subset(int index, int length)
            {
                for (int i = index; i < index + length; i++)
                    yield return names[i];
            }
            
            public static void Main(string[] args)
            {
                MusicTitles titles = new MusicTitles();
                Console.WriteLine("原型");
                foreach (string title in titles)
                {
                    Console.WriteLine(title);
                }
                Console.WriteLine();
                Console.WriteLine("反转");
                foreach (string title in titles.Reverse())
                {
                    Console.WriteLine(title);
                }
                Console.WriteLine();
                Console.WriteLine("子集");
                foreach (string title in titles.Subset(2, 2))
                {
                    Console.WriteLine(title);
                }
                Console.ReadKey();
            }
        }
    }
}
