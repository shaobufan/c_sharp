﻿namespace Example_1
{
    public class Average
    {
        public static void Main(string[] args)
        {
            Double n1 = 0, n2 = 0, avg = 0;
            Console.Write("输入数字:");
            if (double.TryParse(Console.ReadLine(), out n1) && double.TryParse(Console.ReadLine(), out n2))
            {
                avg = (n1 + n2) / 2;
            }
            Console.WriteLine("{0}与{1}的平均值为:{2}", n1, n2, avg);
        }
    }
}