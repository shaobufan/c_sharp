using System.Collections;
namespace ForeachStack
{
    public class foreachStack
    {
        public foreachStack()
        {
            Stack s = new Stack();
            s.Push("red");
            s.Push("blue");
            s.Push("green");
            s.Push(2.0);
            s.Push('b');

            Console.WriteLine(s.Peek());

            s.Pop();
            Console.WriteLine(s.Pop());

            if (s.Contains('b'))
                Console.WriteLine("字母b在栈中");

            foreach (object o in s)
                Console.WriteLine(o);

            s.Clear();
        }
    }
}