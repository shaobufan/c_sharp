using System.Collections;
namespace ForeachQueue
{
    public class foreachQueue
    {
        public foreachQueue()
        {
            Queue q = new Queue();
            q.Enqueue("cat");
            q.Enqueue("dog");
            q.Enqueue(1);
            q.Enqueue(2.0);
            q.Enqueue('b');

            Console.WriteLine(q.Peek());

            q.Dequeue();
            Console.WriteLine(q.Dequeue());

            if (q.Contains('b'))
                Console.WriteLine("字母b在队列中");

            foreach (object o in q)
                Console.WriteLine(o);

            q.Clear();
        }
    }
}