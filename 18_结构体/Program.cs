﻿using System;
public class Example
{
    public enum Egender { Male, Female };
    public struct Student
    {
        public bool Ismarry;
        public Egender gender;
        public string Name;
        public int Age;
        public string ID;
    };
    public static void AscendingPrint(Student[] stu, int n)
    {
        Console.WriteLine("==================================");
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine("学号:{0}\t姓名:{1}\t年龄:{2}\t性别:{3}\t婚姻:{4}", stu[i].ID, stu[i].Name, stu[i].Age, stu[i].gender, stu[i].Ismarry);
        }
    }
    public static void DescendingPrint(Student[] stu, int n)
    {
        Console.WriteLine("================================");
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine("学号:{0}\t姓名:{1}\t年龄:{2}\t性别:{3}\t婚姻:{4}", stu[i].ID, stu[i].Name, stu[i].Age, stu[i].gender, stu[i].Ismarry);
        }
    }
    public static void BubbleSort(ref Student[] stu, int n)
    {
        Student temp;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n - i - 1; j++)
            {
                if (stu[j].ID!.CompareTo(stu[j + 1].ID) < 0)
                {
                    temp.Name = stu[j + 1].Name;
                    temp.Age = stu[j + 1].Age;
                    temp.ID = stu[j + 1].ID;

                    stu[j + 1].Name = stu[j].Name;
                    stu[j + 1].Age = stu[j].Age;
                    stu[j + 1].ID = stu[j].ID;

                    stu[j].Name = temp.Name;
                    stu[j].Age = temp.Age;
                    stu[j].ID = temp.ID;
                }
            }
        }
    }
    public static void Main(string[] args)
    {
        Student[] stu = new Student[10];
        int n;
        int i;

        Console.WriteLine("Please enter tne number of student:");
        bool b = int.TryParse(Console.ReadLine(), out n);
        for (i = 0; i < n; i++)
        {
            Console.WriteLine("Please enter NO {0}.name:", i);
            stu[i].Name = Console.ReadLine()!;
            Console.WriteLine("Please enter NO {0}.Age:", i);
            stu[i].Age = int.Parse(Console.ReadLine()!);
            Console.WriteLine("Please enter NO{0}.ID:", i);
            stu[i].ID = Console.ReadLine()!;
            Console.WriteLine("Please enter NO{0}.gender:", i);
            stu[i].gender = (Egender)Enum.Parse(typeof(Egender), Console.ReadLine()!);
            Console.WriteLine("Please enter NO{0}.Ismarry:", i);
            stu[i].Ismarry = bool.Parse(Console.ReadLine()!);
        }
        BubbleSort(ref stu, n);

        AscendingPrint(stu, n);
        DescendingPrint(stu, n);
    }
}