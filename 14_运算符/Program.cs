﻿namespace program
{
    class Example
    {
        public static void moduloOperation()
        {
            Console.WriteLine(5 % 2);
            Console.WriteLine(-5 % 2);
            Console.WriteLine(5.0 % 2.2);
            Console.WriteLine(5.0m % 2.2m);
            Console.WriteLine(-5.2 % 2.0);
        }
        public static void bitOperation()
        {
            Console.WriteLine(true & false);
            Console.WriteLine(true & true);
            Console.WriteLine("{0},{1}", 0xf8, 0x3f);
            Console.WriteLine("0x{0}", 0xf8 & 0x3f);
            Console.WriteLine("0x{0:x}", 0xf8 & 0x3f);
            Console.WriteLine("0x{0:x8}", 0xf8 & 0x3f);
        }
        public static void bitShiftOperation()
        {
            int i = 1;
            long l = 1;
            Console.WriteLine("0x{0:x}", i << 1);
            Console.WriteLine("0x{0:x}", i << 33);
            Console.WriteLine("0x{0:x}", l << 33);
        }
        public static void Main(string[] args)
        {
            moduloOperation();
            bitOperation();
            bitShiftOperation();
        }
    }
}