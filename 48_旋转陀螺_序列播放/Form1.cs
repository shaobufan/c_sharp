namespace _48_旋转陀螺_序列播放
{
    public partial class Form1 : Form
    {
        int tag = 0;
        int st = 1;
        Queue<Image> fImages;
        Stack<Image> SImages1;
        Stack<Image> SImages2;
        Bitmap lImages;
        public Form1()
        {
            InitializeComponent();
            fImages = new Queue<Image>();
            SImages1 = new Stack<Image>();
            SImages2 = new Stack<Image>();
            /*Bitmap lImages = Properties.Resources.Images;
            for (int i = 0; i < 18; i++)
            {
                Rectangle r = new Rectangle(i * 64, 0, 64, 60);
                fImages.Enqueue(lImages.Clone(r, lImages.PixelFormat));
            }*/
            try
            {
                Bitmap lImages1 = Properties.Resources.numbers;

                lImages = new Bitmap(lImages1, 400, 300);
            }
            catch
            {
                if (lImages == null)
                    MessageBox.Show("error");
            }
        }

        private void tmrMain_Tick(object sender, EventArgs e)
        {
            if (tag == 0)
            {
                Image lGyro = fImages.Dequeue();
                picGyro.Image = lGyro;
                fImages.Enqueue(lGyro);
            }
            else
            {
                switch (st)
                {
                    case 1:
                        {
                            Image lGyro = SImages1.Pop();
                            picGyro.Image = lGyro;
                            SImages2.Push(lGyro);
                            if (SImages1.Count <= 0)
                            {
                                st = 2;
                            }
                            break;
                        }
                    case 2:
                        {
                            Image lGyro = SImages2.Pop();
                            picGyro.Image = lGyro;
                            SImages2.Push(lGyro);
                            if (SImages2.Count <= 0)
                            {
                                st = 1;
                            }
                            break;
                        }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 5; j++)
                {
                    Rectangle r = new(j * 80, i * 150, 80, 150);
                    fImages.Enqueue(lImages.Clone(r, lImages.PixelFormat));
                }
            tag = 0;
            tmrMain.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tmrMain.Stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < 2; i++)
                for(int j = 0; j < 5; j++)
                {
                    Rectangle r = new(j * 80, i * 150, 80, 150);
                    SImages1.Push(lImages.Clone(r, lImages.PixelFormat));
                }
            tag = 1;
            tmrMain.Start();
        }
    }
}