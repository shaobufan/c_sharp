﻿using System;
namespace Example
{
    public class Program
    {
        
        public static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    double result = (i + i * 10) * 100 + j + j * 10;
                    if (Math.Floor(Math.Sqrt(result)) == Math.Sqrt(result) && result != 0.0)
                    {
                        Console.WriteLine(result);
                    }
                }
            }
        }
    }
}