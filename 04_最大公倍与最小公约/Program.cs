﻿namespace program
{
    class Example
    {
        public static void Main(string[] args)
        {
            int m, n, num1, num2, temp;
            Console.WriteLine("请输入两个数：");
            num1 = int.Parse(Console.ReadLine() ?? "1");
            num2 = int.Parse(Console.ReadLine() ?? "1");
            m = num1;
            n = num2;
            while (num2 != 0)
            {
                temp = num1 % num2;
                num1 = num2;
                num2 = temp;
            }
            Console.WriteLine("最大公约数是:{0}\n最小公倍数是:{1}", num1, m * n / num1);
        }
    }
}