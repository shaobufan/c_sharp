﻿using System;
namespace Example
{
    public class example
    {
        public static void Main(string[] args)
        {
            String[] myArr = { "fox", "lazy", "dog" };

            Console.WriteLine("初始值:");
            PrintArrayValues(myArr);
            
            Array.Resize(ref myArr, myArr.Length + 2);
            myArr[3] = "cat";
            myArr[4] = "bat";

            Console.WriteLine("调整为较大的长度后的值:");
            PrintArrayValues(myArr);
            
            Array.Resize(ref myArr, 4);
            Console.WriteLine("调整为较小的长度后的值:");
            PrintArrayValues(myArr);
        }
        public static void PrintArrayValues(string[] myArr)
        {
            for (int i = 0; i < myArr.Length; i++)
            {
                Console.WriteLine("[{0}]:{1}", i, myArr[i]);
            }
        }
    }
}
