﻿using System;
namespace Example
{
    public class foreach_test
    {
        public static void Main(string[] args)
        {
            int[][] jagged = { new int[] { 1, 2 }, new int[] { 3, 4, 6 }, new int[] { 4, 5, 6 } };
            int i = jagged[0][0];
            foreach (var row in jagged)
            {
                foreach (var element in row)
                    Console.Write("{0},", element);
                Console.WriteLine();
            }
            foreach (var row in jagged)
            {
                foreach (var element in row)
                    if (element < i)
                        i = element;
            }
            Console.WriteLine("最小为:{0}", i);
        }
    }
}