# C# 笔记

## **一、C#运行环境**

依赖于*dotnet/.net*运行,*Framework64*调用

### **1.1 环境配置**

环境路径配置(略)

### **1.2 文件结构**

项目重点分为*Program.cs*与*工程描述*文件

### **1.3 运行框架**

```cs
using System;//引入命名空间
namesapce Example//定义命名空间
{
    class Program//面向对象,全封装特性
    {
        public static void Main()//主函数,程序入口
        { 
            //此处书写代码
        }
    }
}
```

## **二、关键字及常用方法**

### **2.1 输出语句**

```cs
Console.WriteLine();
```

参数的形式：String int double char 表达式 等等

使用方法：

```cs
Console.WriteLine("输出:{0},{1},{2}", 1, 2, 3);
//结果为 输出:1,2,3
int a = 3;
Console.WriteLine($"输出:{1},{1+1},{a}");
//结果为 输出:1,2,3
```

### **2.2 输入语句**

```cs
Console.Read();
Console.ReadLine();
```

C#的输出语句读入的均为字符，Read读入单个字符、ReadLine读入一行字符。**注意：** 使用Console.ReadLine()时需注意当程序运行至此行时按下 Ctrl + z 将返回NULL，所以当程序涉及空值敏感时应注意。

### **2.3 类型转换**

```cs
int = int.Parse(/* 其他值 */);
double = double.Parse(/* 其他值 */);
```

由于C#输入语句仅支持字符输入，故方法常与输入语句配套使用。

### **2.4 关于字符串的处理**

#### **2.4.1 StringBuilder**

```cs
StringBuilder sb = new StringBuilder();
sb.Append("string1");
sb.AppendLine("string2");
sb.Append("string3");
string s = sb.ToString();
Console.WriteLine(s);
/*运行结果为
string1string2
string3
*/
```

StringBuilder本身是一个类，所以使用前先引入System.Text命名空间、实例化类。顾名思义此类用于构建字符串，此类存在的意义在于传统定义下的字符串不具有二次重构的功能，想要更新字符串就要开辟新的变量，如果想要根据不同的情况定义字符串就十分麻烦。StringBuilder类应运而生。


使用方法：
1、实例化类
2、使用Append函数追加构建字符串
3、利用ToString将类转化为字符串

#### **2.4.2 字符串的合并与拆分**

```cs
string[] sArray1 = { "少", "不", "钒" };
string s1 = string.Join(",", sArray1);
Console.WriteLine(s1);
```

**Join( 需要插入的字符, 目标字符串数组 )** 函数会将字符串数组的数据项之间添加特定的字符后转化为字符串输出。

```cs
string s2 = "少 不;钒,陈";
string[] sArray3 = s2.Split(',', ';', ' ');
Console.WriteLine(string.Join(Environment.NewLine, sArray3));
```

**Split(识别字符)** 函数会根据定义的识别字符将字符串分割为字符串数组

#### **2.4.3 转义字符**

```cs
string str1 = "你是\"王三\"?";
string str2 = "星期一\\星期二\\星期三";
string str3 = "你好!\t王三。";
Console.WriteLine("str1={0},str2={1},str3={2}", str1, str2, str3);
/*输出结果:
str1=你是"王三"?,str2=星期一\星期二\星期三,str3=你好!   王三。
*/
```

**使用场景** : 在使用输出语句时介于字符的配对原则,例如 *"你是"王三"?"* 如果直接输出程序将无法配对 *""* 此时就需要将输出的""与程序识别的""区分开。转义字符 \t 、\ \、 \ " 应运而生。*( 这种解释并不完全正确 )*


然而另一个问题随之而来：如果想直接输出转义字符如何实现？如图所示:

```cs
string str4 = @"你好\t王三\?";
string str5 = @"星期一\\星期二\\星期三";
Console.WriteLine("str3={0},str4={1}", str4, str5);
/*输出结果:
str3=你好\t王三\?,str4=星期一\\星期二\\星期三
*/
```