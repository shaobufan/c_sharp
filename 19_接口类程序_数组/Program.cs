﻿using System;
namespace Example
{
    public class example
    {
        interface OpInt
        {
            int GetMax();
        }
        class Finder : OpInt
        {
            public int[] Datas = { 4, 7, 8, 2, 3 };
            public int GetMax()
            {
                int temp = Datas[0];
                for (int i = 0; i < Datas.Length; i++)
                {
                    if (temp <= Datas[i])
                    {
                        temp = Datas[i];
                    }
                }
                return temp;
            }
            public double GetAverage()
            {
                double sum = 0;
                for (int i = 0; i < Datas.Length; i++)
                {
                    sum += Datas[i];
                }
                return sum / Datas.Length;
            }
        }
        public class program
        {
            public static void Main(string[] args)
            {
                Finder f = new Finder();
                int Max = f.GetMax();
                double Average = f.GetAverage();
                Console.WriteLine("最大值:{0}\n平均值:{1}", Max, Average);
            }
        }
    }
}