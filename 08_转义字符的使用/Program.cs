﻿//using System;
class Example
{
    public static void Main()
    {
        string str1 = "你是\"王三\"?";
        string str2 = "星期一\\星期二\\星期三";
        string str3 = "你好!\t王三。";
        string str4 = @"你好\t王三\?";
        string str5 = @"星期一\\星期二\\星期三";
        Console.WriteLine("str1={0},str2={1},str3={2}", str1, str2, str3);
        Console.WriteLine("str3={0},str4={1}", str4, str5);
    }
}