﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _45_窗体显示位置及其外观控制
{
    public partial class Form2 : Form
    {
        bool isCustomStyle = false;
        public Form2()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            label1.Text = "当前居中";
        }
        public Form2(int screenX,int screenY)
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.Manual;
            Point p = new Point(screenX, screenY);
            label1.Text = "当前窗体的屏幕坐标位置:" + p.ToString();
            this.Location = p;
        }
        public Form2(bool isCustomStyle)
        {
            this.isCustomStyle = isCustomStyle;
            InitializeComponent();
            if(isCustomStyle)
            {
                this.FormBorderStyle = FormBorderStyle.None;
                this.BackColor = Color.BurlyWood;
                this.StartPosition = FormStartPosition.CenterScreen;
                label1.Text = "异型";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            if(isCustomStyle)
            {
                System.Drawing.Drawing2D.GraphicsPath formShape = new System.Drawing.Drawing2D.GraphicsPath();
                formShape.AddEllipse(0,0,this.Width,this.Height);
                this.Region = new System.Drawing.Region(formShape);
            }
        }
    }
}
